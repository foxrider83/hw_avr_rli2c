# Hw Avr Rli2c

## Presentation
This board is an interface between relay board and a raspberry pi.
The AVR microcontroller decodes the i2c commands and controls the relays.
The board have the capability to be used with TTL and/or LVTTL I2C signals.
The board is designed with [Kicad 6](https://www.kicad.org)

## License
Open Hardware Licence.

## Project status
This project is in progress.
